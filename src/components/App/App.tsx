import React, { useState } from "react";
import { nanoid } from "nanoid";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Grid } from "@mui/material";
import Header from "../Header/Header";
import AddTodo from "../AddTodo/AddTodo";
import FilterTodo from "../FilterTodo/FilterTodo";
import TodoList from "../TodoList/TodoList";
export type Todo = {
  id: string;
  content: string;
  isCompleted: boolean;
  isImportant: boolean;
};
export type Filters = "All" | "Important" | "Completed";

export const TodosContext = React.createContext<Todo[] | null>(null);

function App() {
  const FILTER_MAP = {
    All: () => true,
    Important: (todo: Todo) => todo.isImportant,
    Completed: (todo: Todo) => todo.isCompleted,
  };

  const FILTER_NAMES: any = Object.keys(FILTER_MAP);

  const [todos, setTodos] = useState<Todo[]>([]);

  const [isEmptyInput, setIsEmptyInput] = useState<boolean>(false);

  const [filter, setFilter] = useState<Filters>("All");

  const [search, setSearch] = useState<string>("");

  const handleTodo = (
    content: string,
    e?: React.FormEvent<HTMLFormElement>
  ): void => {
    if (e) e.preventDefault();

    if (content.trim() === "") {
      setIsEmptyInput(true);
    } else {
      setIsEmptyInput(false);
      const todo = {
        id: nanoid(),
        content,
        isCompleted: false,
        isImportant: false,
      };
      const newTodos = [...todos, todo];
      setTodos(newTodos);
    }
  };

  const handleDelete = (id: string): void => {
    setTodos((todos) => todos.filter((todo) => todo.id !== id));
  };

  const handleImportant = (id: string): void => {
    const newTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.isImportant = !todo.isImportant;
      }
      return todo;
    });
    setTodos(newTodos);
  };

  const handleComplete = (id: string): void => {
    const newTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.isCompleted = !todo.isCompleted;
      }
      return todo;
    });
    setTodos(newTodos);
  };

  const handleEdit = (content: string, id: string): void => {
    const newTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.content = content;
      }
      return todo;
    });
    setTodos(newTodos);
  };

  const darkTheme = createTheme({
    palette: {
      mode: "dark",
    },
  });

  const closeError = (): void => setIsEmptyInput(false);

  const taskList =
    search.length > 0
      ? todos
          .filter((todo) =>
            todo.content
              .toLocaleLowerCase()
              .includes(search.toLocaleLowerCase())
          )
          .filter(FILTER_MAP[filter])
      : todos.filter(FILTER_MAP[filter]);

  return (
    <TodosContext.Provider value={taskList}>
      <ThemeProvider theme={darkTheme}>
        <Grid
          container
          spacing={0}
          gap="10px"
          direction="column"
          alignItems="center"
          justifyContent="center"
        >
          <Header />
          <AddTodo
            handleTodo={handleTodo}
            isEmptyInputError={isEmptyInput}
            closeError={closeError}
          />
          <FilterTodo
            setSearch={setSearch}
            filter={filter}
            setFilter={setFilter}
            filterNames={FILTER_NAMES}
          />
          <TodoList
            handleEdit={handleEdit}
            handleComplete={handleComplete}
            handleDelete={handleDelete}
            handleImportant={handleImportant}
          />
        </Grid>
      </ThemeProvider>
    </TodosContext.Provider>
  );
}

export default App;
