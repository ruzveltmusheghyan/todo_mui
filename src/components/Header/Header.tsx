import { Typography } from "@mui/material";
import { Container } from "@mui/system";
import {
  responsiveFontSizes,
  createTheme,
  ThemeProvider,
} from "@mui/material/styles";

import React from "react";

const Header: React.FC = (): JSX.Element => {
  let theme = createTheme();
  theme = responsiveFontSizes(theme);
  return (
    <ThemeProvider theme={theme}>
      <header>
        <Container maxWidth="sm" sx={{ textAlign: "center" }}>
          <Typography color="white" variant="h4">
            Hello,
            <span style={{ fontWeight: "bold" }}>Ruzvelt</span>
          </Typography>
        </Container>
      </header>
    </ThemeProvider>
  );
};

export default Header;
