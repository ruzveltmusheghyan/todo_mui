import React, { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import GradeIcon from "@mui/icons-material/Grade";
import GradeOutlinedIcon from "@mui/icons-material/GradeOutlined";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import EditIcon from "@mui/icons-material/Edit";
import SpellcheckIcon from "@mui/icons-material/Spellcheck";
import {
  IconButton,
  Input,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import { Todo } from "../App/App";

interface ITodoItemProps {
  todo: Todo;
  handleComplete: (id: string) => void;
  handleDelete: (id: string) => void;
  handleImportant: (id: string) => void;
  handleEdit: (content: string, id: string) => void;
}

const TodoItem: React.FC<ITodoItemProps> = ({
  todo,
  handleComplete,
  handleDelete,
  handleImportant,
  handleEdit,
}): JSX.Element => {
  const [inEditMode, setInEditMode] = useState<boolean>(false);

  const [newTodo, setNewTodo] = useState<string>("");

  const editTodo = (id: string): void => {
    if (inEditMode) {
      handleEdit(newTodo, id);
      setInEditMode(false);
    } else {
      setInEditMode(true);
    }
  };

  return (
    <List key={todo.id}>
      <ListItem sx={{ padding: 0 }}>
        {inEditMode ? (
          <Input
            onChange={(e) => setNewTodo(e.target.value)}
            fullWidth
            defaultValue={todo.content}
          />
        ) : (
          <ListItemText
            sx={{
              color: "white",
              textDecoration: todo.isCompleted ? "line-through" : "",
            }}
            primary={todo.content}
          ></ListItemText>
        )}
        <ListItemIcon sx={{ cursor: "pointer", minWidth: 0 }}>
          <IconButton onClick={() => handleComplete(todo.id)}>
            {todo.isCompleted ? <CheckBoxIcon /> : <CheckBoxOutlineBlankIcon />}
          </IconButton>
        </ListItemIcon>
        <ListItemIcon sx={{ cursor: "pointer", minWidth: 0 }}>
          <IconButton onClick={() => handleDelete(todo.id)}>
            <DeleteIcon />
          </IconButton>
        </ListItemIcon>
        <ListItemIcon sx={{ cursor: "pointer", minWidth: 0 }}>
          <IconButton onClick={() => handleImportant(todo.id)}>
            {todo.isImportant ? <GradeIcon /> : <GradeOutlinedIcon />}
          </IconButton>
        </ListItemIcon>
        <ListItemIcon sx={{ cursor: "pointer", minWidth: 0 }}>
          <IconButton onClick={() => editTodo(todo.id)}>
            {inEditMode ? <SpellcheckIcon /> : <EditIcon />}
          </IconButton>
        </ListItemIcon>
      </ListItem>
    </List>
  );
};

export default TodoItem;
