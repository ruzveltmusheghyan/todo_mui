import React from "react";
import { Box, Button, Input, InputAdornment } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { Filters } from "../App/App";

interface IFilterTodo {
  filterNames: Filters[];
  filter: Filters;
  setFilter: (name: Filters) => void;
  setSearch: (text: string) => void;
}

const FilterTodo: React.FC<IFilterTodo> = ({
  setFilter,
  filter,
  filterNames,
  setSearch,
}): JSX.Element => {
  return (
    <Box display="flex" flexDirection="column" gap={2}>
      <Box
        minWidth="360px"
        mt={5}
        display="flex"
        justifyContent="space-between"
      >
        {filterNames.map((name) => {
          const isPressed = name === filter;
          return (
            <Button
              variant={isPressed ? "contained" : "outlined"}
              onClick={() => setFilter(name)}
              key={name}
            >
              {name}
            </Button>
          );
        })}
      </Box>
      <Box>
        <Input
          fullWidth
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
          endAdornment={
            <InputAdornment position="end">
              <SearchIcon />
            </InputAdornment>
          }
        />
      </Box>
    </Box>
  );
};

export default FilterTodo;
