import React, { useState } from "react";
import { Box } from "@mui/system";
import { Input, InputAdornment } from "@mui/material";
import AddCircleIcon from "@mui/icons-material/AddCircle";

import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

interface IAddTodoProps {
  handleTodo: (content: string, e?: React.FormEvent<HTMLFormElement>) => void;
  isEmptyInputError: boolean;
  closeError: () => void;
}

const AddTodo: React.FC<IAddTodoProps> = ({
  handleTodo,
  isEmptyInputError,
  closeError,
}): JSX.Element => {
  const [text, setText] = useState<string>("");

  return (
    <Box mt={10} maxWidth="xl" display="flex" alignItems="end" gap="15px">
      <Snackbar
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        open={isEmptyInputError}
        autoHideDuration={6000}
        onClose={closeError}
      >
        <Alert onClose={closeError} severity="error" sx={{ width: "100%" }}>
          Todo cannot be empty
        </Alert>
      </Snackbar>
      <form onSubmit={(e) => handleTodo(text, e)}>
        <Input
          value={text}
          placeholder="Add todo..."
          sx={{ minWidth: "360px" }}
          onChange={(e) => setText(e.target.value)}
          endAdornment={
            <InputAdornment
              onClick={() => handleTodo(text)}
              sx={{ cursor: "pointer" }}
              position="end"
            >
              <AddCircleIcon />
            </InputAdornment>
          }
        />
        <Input type="submit" value="" />
      </form>
    </Box>
  );
};

export default AddTodo;
