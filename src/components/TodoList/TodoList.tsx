import { Box, Typography } from "@mui/material";
import React, { useContext } from "react";
import { TodosContext } from "../App/App";
import TodoItem from "../TodoItem/TodoItem";

interface ITodoItem {
  handleComplete: (id: string) => void;
  handleDelete: (id: string) => void;
  handleImportant: (id: string) => void;
  handleEdit: (content: string, id: string) => void;
}

const TodoList: React.FC<ITodoItem> = ({
  handleComplete,
  handleDelete,
  handleImportant,
  handleEdit,
}): JSX.Element => {
  const todos = useContext(TodosContext);

  return (
    <Box minWidth="360px" mt={5}>
      {todos && todos.length > 0 ? (
        todos.map((todo) => {
          return (
            <TodoItem
              handleEdit={handleEdit}
              handleComplete={handleComplete}
              handleDelete={handleDelete}
              handleImportant={handleImportant}
              key={todo.id}
              todo={todo}
            />
          );
        })
      ) : (
        <Typography variant="body2" color="white" fontWeight="bold">
          No todos...
        </Typography>
      )}
    </Box>
  );
};

export default TodoList;
